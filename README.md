# Test task for the backend developer position 

## Feature requirements

- There are many publishers, books and shops
- Publishers produce books that are sold in shops
- A book can be distributed via multiple shops
- A shop can have in stock and sell multiple copies of a book

## What should be done

- Model the required database relations
- Create API Endpoints described below
- Covered your code with tests using RSpec
- Provide seed data to play with your solution

## API Requirements

### Endpoint 1

For a specific publisher, return the list of shops selling at least one book of that publisher. Shops should be ordered by the number of books sold (top sellers come first). Each shop should include the list of books of this same publisher that are currently in stock.

Example response:

```json
{ 
  "shops": [
   {
     "id": 1,
     "name": "Amazon",
     "books_sold_count": 10,
     "books_in_stock": [
       {
         "id": 2,
         "title": "Yiddish songs",
         "copies_in_stock": 3
       },
       … 
     ]
   },
   … 
 ]
}
```

### Endpoint 2

For a specific shop, record a sale of one or multiple copies of a book.

## Comments

Please upload your code to a Github or Gitlab repository (there is no need in ZIP archives or smth) and send us the link to your solution.

If you have any questions, don't hesitate to ask the person who shared this test with you.
